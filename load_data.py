"""Functions to load data and infer dimensions"""
import numpy as np

class WEPLData:
    rawCBCT = []
    corrCBCT = []
    manCT = []
    rigidCT = []
    deformCT = []

def guess_xdim(datalength):
    xdim = 0
    if datalength % (512 - 4) == 0:
        xdim = 512 - 4
    elif datalength % 297 == 0:
        xdim = 297
    else:
        print(datalength)
    return xdim

def load_single_modality_from_wepl_file(filename, column=3):
    data = np.loadtxt(filename, skiprows=1, usecols=column)
    datalength = len(data)
    xdim = guess_xdim(datalength)
    return np.reshape(data, (-1, xdim))

def load_wepl_file(filename):
    data = np.loadtxt(filename, skiprows=1, usecols=(3,4,5,6,7))
    datalength = len(data[:, 0])
    xdim = guess_xdim(datalength)

    wepl_data = WEPLData()
    wepl_data.rawCBCT = np.reshape(data[:, 0], (-1, xdim))
    wepl_data.corrCBCT = np.reshape(data[:, 1], (-1, xdim))
    wepl_data.manCT = np.reshape(data[:, 2], (-1, xdim))
    wepl_data.rigidCT = np.reshape(data[:, 3], (-1, xdim))
    wepl_data.deformCT = np.reshape(data[:, 4], (-1, xdim))
    return wepl_data
